package src.main.java

object AppDetails {

    const val APP_ID = "scott.app.~replacePkgName~"
    const val APP_VERSION_NAME = "1.0.0"
    const val APP_VERSION_CODE = 1
}