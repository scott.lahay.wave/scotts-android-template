package src.main.java

object Apps {
    const val compileSdk = 29
    const val minSdk = 21
    const val targetSdk = 29
    const val versionCode = 1
    const val versionName = "1.0.0"
}

object Versions {
    const val gradle = "3.5.0"
    const val kotlin = "1.3.50"
    const val appcompat = "1.0.2"
    const val compose = "1.0.0-alpha06"

    /* test */
    const val junit = "4.13.1"
}

object Libs {
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlin}"
    const val appcompat = "androidx.appcompat:appcompat:${Versions.appcompat}"
    const val ktx = "androidx.core:core-ktx:1.3.2"
    const val material = "com.google.android.material:material:1.2.1"
    const val compose = "androidx.compose.ui:ui:${Versions.compose}"
    const val composeMaterial = "androidx.compose.material:material:${Versions.compose}"
    const val tooling = "androidx.ui:ui-tooling:${Versions.compose}"
    const val lifecycleKtx = "androidx.lifecycle:lifecycle-runtime-ktx:2.3.0-beta01"
    const val junit = "junit:junit:4.13.1"
    const val espresso = "androidx.test.espresso:espresso-core:3.3.0"
    const val androidxJUnit = "androidx.test.ext:junit:1.1.2"
}

object TestLibs {
    const val junit = "junit:junit:${Versions.junit}"
}