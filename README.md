# Scotts Project Template

This is a project template, that sets up all the things I like

## Overview
* Gradle.build files are written in Kotlin and use KTS
* Uses Jetpack Compose
* Adds ScottUtils.kt for my basic utils
* Adds a color.kt with a bunch of pre built colors
* Theme is ScottTheme.kt

## Steps after pulling template
**GitLab**
1. Give it a project name
1. Copy the project name for pasting
1. Add a readme

**File Explorer Open C:\\MyGitRepo**
1. Edit newProjectCreator.bat
1. Paste project name into project name

**CMD**
1. cd C:\\MyGitRepo
1. Run newProjectCreator.bat

**Android Studio**
1. Open project
1. App -> Java select package - rename package
1. Find/replace ~replacePkgName~
1. Find/replace ~replaceAppName~
1. Verify The basics work by running the project in the emulator
1. Commit the project to git
1. Verify project is on GitLab