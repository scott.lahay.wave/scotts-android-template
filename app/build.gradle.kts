import src.main.java.AppDetails
import src.main.java.Libs

plugins {
    id("com.android.application")
    id("kotlin-android")
}

android {
    compileSdkVersion(30)

    defaultConfig {
        applicationId = AppDetails.APP_ID
        minSdkVersion(30)
        targetSdkVersion(30)
        versionCode = AppDetails.APP_VERSION_CODE
        versionName = AppDetails.APP_VERSION_NAME

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("debug") {
            isMinifyEnabled = false
            isDebuggable = true
            applicationIdSuffix = ".debug"
            versionNameSuffix = "-dev"
//            signingConfig = signingConfigs.getByName("debug")
        }
        getByName("release") {
            isMinifyEnabled = true
            isDebuggable = false
            isShrinkResources = true
            isJniDebuggable = false
            isRenderscriptDebuggable = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
//            signingConfig = signingConfigs.getByName("release")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
        useIR = true
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.0.0-alpha04"
        kotlinCompilerVersion = "1.4.10"
    }
}

dependencies {

    implementation(Libs.kotlin)
    implementation(Libs.appcompat)
    implementation(Libs.ktx)
    implementation(Libs.material)
    implementation(Libs.compose)
    implementation(Libs.composeMaterial)
    implementation(Libs.tooling)
    implementation(Libs.lifecycleKtx)
    testImplementation(Libs.junit)
    androidTestImplementation(Libs.espresso)
    androidTestImplementation(Libs.androidxJUnit)
}