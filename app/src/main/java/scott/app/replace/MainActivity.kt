package scott.app.replace

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Text
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.setContent
import androidx.ui.tooling.preview.Preview
import scott.app.replace.ui.ScottTheme

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ScottTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Greeting("~replaceAppName~")
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Scott template app says welcome to $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ScottTheme {
        Greeting("~replaceAppName~")
    }
}